
all: out/aero-mavlink-router

clean:
	rm -rf out build

build/aero-mavlink-router/src/mavlink-routerd:
	meson setup build/aero-mavlink-router mavlink-router
	ninja -C build/aero-mavlink-router

out/aero-mavlink-router: build/aero-mavlink-router/src/mavlink-routerd
	mkdir -p out/aero-mavlink-router
	cp build/aero-mavlink-router/src/mavlink-routerd out/aero-mavlink-router
	cp build/aero-mavlink-router/mavlink-router.service out/aero-mavlink-router
	cp meta-intel-aero/recipes-support/mavlink-router/files/main.conf out/aero-mavlink-router
	cp meta-intel-aero/recipes-support/mavlink-router/files/mavlink-routerd.sh out/aero-mavlink-router
	patch out/aero-mavlink-router/mavlink-router.service -i meta-intel-aero/recipes-support/mavlink-router/files/0001-Set-rx-trigger-on-ttyS1.patch
